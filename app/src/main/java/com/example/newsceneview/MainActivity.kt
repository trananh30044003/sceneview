package com.example.newsceneview

import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.IntOffset
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.example.newsceneview.ui.theme.NewSceneViewTheme
import com.google.android.filament.Filament
import com.google.android.filament.android.UiHelper
import com.google.android.filament.utils.HDRLoader
import com.google.android.filament.utils.Manipulator
import com.google.ar.core.Camera
import io.github.sceneview.Scene
import io.github.sceneview.SceneView
import io.github.sceneview.environment.loadEnvironmentAsync
import io.github.sceneview.gesture.NodeMotionEvent
import io.github.sceneview.gesture.RotateGestureDetector

import io.github.sceneview.math.Position
import io.github.sceneview.math.Rotation
import io.github.sceneview.math.Scale
import io.github.sceneview.node.CameraNode2
import io.github.sceneview.node.ModelNode
import io.github.sceneview.node.Node
import io.github.sceneview.utils.FrameTime
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DateFormat.FULL


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NewSceneViewTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ModelScreen(lifecycle = this.lifecycleScope)
                }
            }
        }
    }
}

@Composable
fun ModelScreen(lifecycle: LifecycleCoroutineScope) {
    val nodes = remember { mutableStateListOf<Node>() }
    val context = LocalContext.current
    var offset by remember { mutableStateOf(IntOffset.Zero) }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.White)
//            .pointerInput(Unit) {
//                detectDragGestures { change, dragAmount ->
//                    offset += dragAmount.round()
////                        change.consumePositionChange()
//                    Log.e("offset", offset.toString())
//                }
//            }
    ) {
        Scene(
            nodes = nodes,
            onCreate = {
                val model = ModelNode(it.engine)
                val model2 = ModelNode(it.engine)
//                FrameTime(1000, 3000)
//                colorOf(0f, 55f,255f, 1f)
//                val view = CameraNode2(it.engine, false)
////                view.smooth(position = Position(0f, 0f, 0f), scale = Scale(10f, 0f, 0f), rotation = Rotation(), speed = 10f)
//                view.transform(position = Position(-40f, -3f, 0f), scale = Scale(1f, 1f, 1f), rotation = Rotation(0f, 0f, 0f), smooth = true, smoothSpeed = 10f)
//                view.farClipPlane = 10f
//                view.nearClipPlane= 8f
//                SceneView(
//                    context = context,
//                    attrs = null,
//                    defStyleRes = 0,
//                    defStyleAttr = 0,
//                    cameraNode = null,
//                    cameraManipulator = { width, height ->
//                        Manipulator.Builder()
//                            .targetPosition(0f, 0f, 0f)
//                            .viewport(width, height)
//                            .build(Manipulator.Mode.ORBIT)
//                    }
//                )
//
//
//                    })

                val cameras = it.cameraNode
//                FrameTime(10000000000000000, 60000000000000000)
//                FrameTime(0, 166666667)
                cameras.transform(position = Position(0f, 0f, 0f), scale = Scale(1f, 1f, 1f), rotation = Rotation(0f, 0f, 0f), smooth = true, smoothSpeed = 100f)
                cameras.isRotationEditable = true
                cameras.smooth(position = Position(0f, 0f, 0f), scale = Scale(1f, 1f, 1f), rotation = Rotation(0f, 0f, 0f), speed = 100f)
                cameras.smoothSpeed = 10000000f
                HDRLoader.loadEnvironmentAsync(
                    context = context,
                    hdrFileLocation = "environments/studio_small_09_2k.hdr",
                    coroutineScope = lifecycle,
                    specularFilter = true,
                    createSkybox = true,
//                    lifecycle = lifecycle
                ) { hdrEnvironment ->
                    it.environment = hdrEnvironment
                }
                model2.loadModelGlbAsync(
                    glbFileLocation = "models/zoom_4k.glb",
                    autoAnimate = true,
                    scaleToUnits = 7f,
                    centerOrigin = Position(-0.3f, -0.65f, 0.5f),
                    onError = {
//                        Log.e("Error Scene View", it.message.toString())
                    },
                    onLoaded = { modelInstance -> }
                )
                model.loadModelGlbAsync(
                    glbFileLocation = "models/femaleShirt.glb",
                    autoAnimate = false,
                    scaleToUnits = 1f,
                    centerOrigin = Position(0.0f, 0.0f, 0.0f),
                    onError = {
//                        Log.e("Error Scene View", it.message.toString())
                    },
                    onLoaded = { modelInstance -> }
                )
//                GlobalScope.launch {
//                    model.loadModelGlb(
//                        context= context,
//                        glbFileLocation = "models/femaleShirt.glb",
//                        autoAnimate = false,
//                        scaleToUnits = 1f,
//                        centerOrigin = Position(0.0f, 0.0f, 0.0f),
//                        onError = {
////                        Log.e("Error Scene View", it.message.toString())
//                        }
//                    )
////                    nodes.add(model)
//                }

                model.transform(
                    position = Position(0f, 0f, 0f),
                    scale = Scale(1f, 1f, 1f),
                    rotation = Rotation(0f, 0f, 0f),
                    smooth = true,
                    smoothSpeed = 1000f
                )
//                nodes.add(view)
                nodes.add(model)
                nodes.add(model2)

            },
            modifier = Modifier
                .background(color = Color.White),
        )
    }
}